export default interface RichTextEditorOptions {

  /**
   * Operation mode:
   *
   * 'checkbox' - Can select multiple styles
   * 'radio' - Can only select one style at a time
   */
  mode: 'checkbox' | 'radio';

  /**
   * Must be valid `document.execCommand()` commands.
   *
   * Note: Only supports styles that do not need a value argument (i.e. the
   * third parameter in `document.execCommand()`) though that could easily be
   * incorporated. I have not tested all the styles.
   *
   * @see https://developer.mozilla.org/en-US/docs/Web/API/Document/execCommand#Commands
   */
  styles: string[];

  /**
   * Labels for each style
   *
   * HTML is allowed. Examples:
   * <span style="font-weight: bold">Bold</span>
   * <span class="my-bold-class">Bold</span>
   * <i class="fas fa-bold"></i>
   *
   * Must follow order of the `styles` property. All `styles` must have an
   * associated label.
   */
  labels: string[];
}
