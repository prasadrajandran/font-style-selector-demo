# Font Style Selector Demo  

## Introduction  

Built using Vue.js + TypeScript via Vue CLI.  

## Demo  

* [https://demo.prasadpr.com/](https://demo.prasadpr.com/)  

## Update (July 2, 2018)  

Built another version of this using custom elements (i.e. no JS framework)

* [Native Font Style Selector](https://bitbucket.org/prasadrajandran/font-style-selector-native-demo/src/master/)  

## Toggle Component  

* [/src/components/Toggle.vue](https://bitbucket.org/prasadrajandran/font-style-selector-demo/src/master/src/components/Toggle.vue)  

## Other Components

* [/src/components/RichTextEditor.vue](https://bitbucket.org/prasadrajandran/font-style-selector-demo/src/master/src/components/RichTextEditor.vue) -
Rich Text Editor component that utilises the Toggle component. It can be
configured using via the `options` property. Please have a look at the
[RichTextEditorOptions](https://bitbucket.org/prasadrajandran/font-style-selector-demo/src/master/src/interfaces/RichTextEditorOptions.ts)
interface.  

* [/src/components/Demo.vue](https://bitbucket.org/prasadrajandran/font-style-selector-demo/src/master/src/components/Demo.vue) -
To demo the project  
* [/src/App.vue](https://bitbucket.org/prasadrajandran/font-style-selector-demo/src/master/src/App.vue) -
Root component  

## NPM Commands  

Note: [NPM](https://www.npmjs.com/get-npm) must already be installed in order
to utilise the following commands.  

* `npm install` - Installs all dependencies. Run this before running the
commands below.
* `npm run serve` - Runs a "watched" local copy on `localhost:8080`
* `npm run build` - Create production build that gets stored in `/dist`  

## Repository

* [BitBucket](https://bitbucket.org/prasadrajandran/font-style-selector-demo/src/master/)  

## Notes  

* Uses `document.execCommand()` so expect bugs and inconsistencies across
different browsers.

## Resources  

* [Vue.js 2.0 Docs](https://vuejs.org/v2/guide/)  
* [Vue.js CLI Docs](https://vuejs.org/v2/guide/installation.html#CLI)  

## Author  

Prasad Rajandran (prasadpr@outlook.com)
